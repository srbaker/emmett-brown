import {EmmettBrown} from '../EmmettBrown';

describe('EmmettBrown', () => {
    describe('setTime', () => {
        afterEach(() => {
            EmmettBrown.resetTime();
        });

        it('causes new Date() to return the specified date', () => {
            const desiredDate = new Date(1983, 3, 22, 15, 15, 30)
            EmmettBrown.setTime(desiredDate);

            expect(new Date()).toEqual(desiredDate);
        });

        it('causes Date.now() to return the specified date', () => {
            const desiredDate = new Date(1983, 3, 22, 15, 15, 30)
            EmmettBrown.setTime(desiredDate);

            expect(Date.now()).toEqual(desiredDate.getTime());
        });
    });

    describe('resetTime', () => {
        it('starts providing real dates', () => {
            const desiredDate = new Date(1983, 3, 22, 15, 15, 30)
            EmmettBrown.setTime(desiredDate);

            expect(new Date()).toEqual(desiredDate);

            EmmettBrown.resetTime();

            expect(new Date().getTime()).toBeGreaterThan(desiredDate.getTime());
        });
    });

    describe('useDate', () => {
        it('uses the specified time inside the block', () => {
            const desiredDate = new Date(1983, 3, 22, 15, 15, 30)
            EmmettBrown.useDate(desiredDate, () => {
                expect(new Date()).toEqual(desiredDate);
            });
            expect(new Date().getTime()).toBeGreaterThan(desiredDate.getTime());
        });
    });
});
