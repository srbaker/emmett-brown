const OriginalDate = Date;

export class EmmettBrown {
    static setTime(desiredDate: Date) {
        const fakeDate = function (..._args: number[]) {
            return desiredDate;
        };

        fakeDate.now = () => desiredDate.getTime();
        fakeDate.UTC = OriginalDate.UTC;
        fakeDate.parse = OriginalDate.parse;
        fakeDate.prototype = OriginalDate.prototype;

        Date = fakeDate as any;
    }

    static resetTime() {
        Date = OriginalDate;
    }

    static useDate(desiredDate: Date, block: () => void) {
        this.setTime(desiredDate);
        block();
        this.resetTime();
    }
}
