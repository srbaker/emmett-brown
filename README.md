# Emmett Brown

Emmett Brown is a library that make time travel possible.

This is especially useful in testing, when you need to use known dates.

## Simple Usage

Set the Date to a custom date using `setTime(date: Date)`. Every instantiated `Date` will be the specified date.

`Date.now()` is also handled.

```typescript
import {EmmettBrown} from "./EmmettBrown";

const birthday = new Date(1983, 3, 22);
EmmettBrown.setTime(birthday);

console.log(`It is always my birthday! ${new Date()}`);
console.log(`No matter how many times it's called: ${new Date()}`);
console.log(`Even when you use Date.now(): ${Date.now()}`);
```

When you want to stop time traveling, call `resetTime()`:

```typescript
import {EmmettBrown} from "./EmmettBrown";

const birthday = new Date(1983, 3, 22);
EmmettBrown.setTime(birthday);

console.log(`Time traveling to ${new Date()}.`);

EmmettBrown.resetTime();

console.log(`Back in the present: ${new Date()}.`);
```

## Block Usage

The block usage can be nicer because you don't need to clean up after yourself.

```typescript
import {EmmettBrown} from "./EmmettBrown";

EmmettBrown.useDate(new Date(1983, 3, 22), () => {
    console.log(`In here, we're time traveling: ${new Date()}.`);
});

console.log(`Out here, we're stuck in reality: ${new Date()}.`);
```
